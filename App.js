import React from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const App = () => {
  return (
    <SafeAreaView style={style.parentContent}>
      <View style={[{backgroundColor: 'white'}, style.parentContent]}>
        <Text style={style.titleHeader}>Digital Approval</Text>
        <Image style={style.ImageStyle} source={require('./gambar.png')} />
        <TextInput
          style={style.input}
          placeholder="Email"
          keyboardType="words"
        />
        <TextInput
          secureTextEntry={true}
          style={[style.input, {marginTop: 8}]}
          placeholder="Password"
          keyboardType="words"
        />
        <Text style={{marginBottom: 24, alignSelf: 'flex-end'}}>
          Reset Password
        </Text>
        <TouchableOpacity style={style.button}>
          <Text style={{color: '#FFFFFF', fontSize: 16, fontWeight: 'bold'}}>
            LOGIN
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  parentContent: {
    backgroundColor: '#F7F7F7',
    justifyContent: 'center',
    flex: 1,
    padding: 20,
  },
  titleHeader: {
    backgroundColor: '#002558',
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'center',
    paddingVertical: 16,
    paddingHorizontal: 40,
    borderRadius: 30,
    marginTop: -50,
    margin: 27,
  },
  sectionContainer: {
    padding: 24,
    borderRadius: 10,
  },
  ImageStyle: {
    marginBottom: 38,
    alignSelf: 'center',
  },
  input: {
    paddingHorizontal: 16,
    marginBottom: 16,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#002558',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#287ae5',
  },
});

export default App;
